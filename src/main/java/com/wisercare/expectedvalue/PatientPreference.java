package com.wisercare.expectedvalue;

import lombok.Data;

@Data
public class PatientPreference
{
    private Outcome outcome;
    private Double preferenceValue; //how much does this patient care about this particular outcome

    public PatientPreference(Outcome outcome, Double preferenceValue) {
        this.outcome = outcome;
        this.preferenceValue = preferenceValue;
    }
}
